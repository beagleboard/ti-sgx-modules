#!/bin/bash

#https://git.ti.com/gitweb?p=graphics/ti-img-rogue-driver.git;a=summary

ARCH=$(uname -m)
device="j722s"
branch="linuxws/scarthgap/k6.12/24.2.6643903"

#  LD [M]  /opt/github/buildscripts/bb.org/6.1.x/ti-sgx-modules/src/src/binary_j722s_linux_lws-generic_release/target_aarch64/kbuild/pvrsrvkm.ko

if [ -f .builddir ] ; then
	if [ -d ./src ] ; then
		rm -rf ./src || true
	fi

	echo "git clone -b ${branch} https://github.com/rcn-ee/ti-img-rogue-driver.git ./src --depth=1"
	git clone -b ${branch} https://github.com/rcn-ee/ti-img-rogue-driver.git ./src --depth=1

	PVR_SOC=j722s_linux
	PVR_BUILD=release
	PVR_WS=lws-generic
	if [ "x${ARCH}" = "xx86_64" ] ; then
		x86_dir="`pwd`/../../normal-arm64"
		if [ -f `pwd`/../../normal-arm64/.CC ] ; then
			. `pwd`/../../normal-arm64/.CC
			make_options="CROSS_COMPILE=aarch64-linux-gnu- KERNELDIR=${x86_dir}/KERNEL BUILD=${PVR_BUILD} PVR_BUILD_DIR=${PVR_SOC} WINDOW_SYSTEM=${PVR_WS}"
		fi
	else
		make_options="CROSS_COMPILE=aarch64-linux-gnu- KERNELDIR=/build/buildd/linux-src BUILD=${PVR_BUILD} PVR_BUILD_DIR=${PVR_SOC} WINDOW_SYSTEM=${PVR_WS}"
	fi

	cd ./src/build/linux/j722s_linux

	make ARCH=arm64 ${make_options} clean
	echo "make ARCH=arm64 ${make_options}"
	make ARCH=arm64 ${make_options}
fi
#
