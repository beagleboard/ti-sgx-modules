#!/bin/bash

#https://git.ti.com/gitweb?p=graphics/omap5-sgx-ddk-linux.git;a=summary

ARCH=$(uname -m)
device="jacinto6evm"
branch="ti-img-sgx/1.17.4948957/k5.10"

#  LD [M]  /opt/github/buildscripts/bb.org/5.10.x/ti-sgx-modules/src/src/eurasia_km/eurasiacon/binary_omap_linux_xorg_release/target_armhf/kbuild/pvrsrvkm.ko

if [ -f .builddir ] ; then
	if [ -d ./src ] ; then
		rm -rf ./src || true
	fi

	echo "git clone -b ${branch} https://github.com/rcn-ee/ti-omap5-sgx-ddk-linux.git ./src --depth=1"
	git clone -b ${branch} https://github.com/rcn-ee/ti-omap5-sgx-ddk-linux.git ./src --depth=1

	if [ "x${ARCH}" = "xx86_64" ] ; then
		x86_dir="`pwd`/../../normal"
		if [ -f `pwd`/../../normal/.CC ] ; then
			. `pwd`/../../normal/.CC
			make_options="TARGET_PRIMARY_ARCH=target_armhf CROSS_COMPILE=${CC} KERNELDIR=${x86_dir}/KERNEL TARGET_PRODUCT=${device}"
		fi
	else
		make_options="TARGET_PRIMARY_ARCH=target_armhf CROSS_COMPILE= KERNELDIR=/build/buildd/linux-src TARGET_PRODUCT=${device}"
	fi

	cd ./src/eurasia_km/eurasiacon/build/linux2/omap_linux

	make ARCH=arm ${make_options} clean
	echo "make ARCH=arm ${make_options}"
	make ARCH=arm ${make_options}
fi
#
