#!/bin/bash

#https://git.ti.com/gitweb?p=arago-project/meta-ti.git;a=tree;f=recipes-bsp/powervr-drivers;hb=refs/heads/dunfell

ARCH=$(uname -m)
device="j721e"
branch="linuxws/dunfell/k5.10/1.15.6133109_unified_fw_pagesize"

#  LD [M]  /opt/github/bb.org/5.10.x/ti-sgx-omap5/src/src/binary_j721e_linux_wayland_release/target_aarch64/kbuild/pvrsrvkm.ko

if [ -f .builddir ] ; then
	if [ -d ./src ] ; then
		rm -rf ./src || true
	fi

	echo "git clone -b ${branch} https://git.ti.com/git/graphics/ti-img-rogue-driver.git ./src --depth=1"
	git clone -b ${branch} https://git.ti.com/git/graphics/ti-img-rogue-driver.git ./src --depth=1

	PVR_SOC=j721e_linux
	PVR_BUILD=release
	PVR_WS=wayland
	if [ "x${ARCH}" = "xx86_64" ] ; then
		x86_dir="`pwd`/../../normal-arm64"
		if [ -f `pwd`/../../normal-arm64/.CC ] ; then
			. `pwd`/../../normal-arm64/.CC
			make_options="CROSS_COMPILE=aarch64-linux-gnu- KERNELDIR=${x86_dir}/KERNEL RGX_BVNC=22.104.208.318 BUILD=${PVR_BUILD} PVR_BUILD_DIR=${PVR_SOC} WINDOW_SYSTEM=${PVR_WS}"
		fi
	else
		make_options="CROSS_COMPILE=aarch64-linux-gnu- KERNELDIR=/build/buildd/linux-src RGX_BVNC=22.104.208.318 BUILD=${PVR_BUILD} PVR_BUILD_DIR=${PVR_SOC} WINDOW_SYSTEM=${PVR_WS}"
	fi

	cd ./src/build/linux/j721e_linux

	make ARCH=arm64 ${make_options} clean
	echo "make ARCH=arm64 ${make_options}"
	make ARCH=arm64 ${make_options}
fi
#
